import React from 'react'
import './App.css'
import Table from './components/Table'
import tableData from './data'

const App = () => {
   React.useEffect(() => {
      console.log('Hello from App.js!')
   }, [])
   
   return <Table tableData={tableData} />
}

export default App
