import React from "react"

const Table = () => (
    <table>
        <thead>
            <tr>
                <th>First column</th>
                <th>Second column</th>
                <th>Third column</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>One</td>
                <td>Two</td>
                <td>Three</td>
            </tr>
        </tbody>
    </table>
)

export default Table
